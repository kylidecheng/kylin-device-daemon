#include "basewidget.h"
#include "config/xatom-helper.h"
#include "fdframe.h"

#include <QPainter>
#include <QStyleOption>
#include <QDebug>
#include <QApplication>
#include <QScreen>
#include <QPainterPath>
#include <QLabel>
#include <kwindowsystem.h>
#include <kwindowsystem_export.h>
#include <ukuistylehelper/ukuistylehelper.h>




#define THEME_UKFD_TRANS "org.ukui.control-center.personalise"

baseWidget::baseWidget(QWidget *parent) : QWidget(parent)
{
//    setWindowFlags(Qt::FramelessWindowHint); //Qt::ToolTip设置新窗口是一个提示窗口，没有标题栏和窗口边框,设置后独立与窗口存在
    setWindowFlags(Qt::Tool);

//   注释掉透明度设置，固定背景透明度，完全透明。
//   initOpacityGSettings();
    m_curTransOpacity = 0;
    installEventFilter(this);

//    根据新版设计说明取消底框上的文字
//    QLabel *tiplabel = new QLabel(this);
//    tiplabel->move(16,16);
//    tiplabel->setFixedWidth(200);
//    tiplabel->setText(tr("removable storage device"));
//    tiplabel->show();

//    this->setAttribute(Qt::WA_AlwaysShowToolTips);
    this->setAttribute(Qt::WA_TranslucentBackground);
    this->setFocusPolicy(Qt::NoFocus);
    setProperty("useSystemStyleBlur", true);  //毛玻璃效果

    //窗管协议,隐藏上方标题栏
    MotifWmHints hints;
    hints.flags = MWM_HINTS_FUNCTIONS|MWM_HINTS_DECORATIONS;
    hints.functions = MWM_FUNC_ALL;
    hints.decorations = MWM_DECOR_BORDER;
    XAtomHelper::getInstance()->setWindowMotifHint(this->winId(), hints);

    mainWindowVboxLayout = new QVBoxLayout();
    mainWindowVboxLayout->setAlignment(Qt::AlignCenter|Qt::AlignBottom);
    mainWindowVboxLayout->setContentsMargins(0,0,0,0);
    mainWindowVboxLayout->setSpacing(7);
    this->setLayout(mainWindowVboxLayout);
    this->adjustSize();

    m_hideTimer = new QTimer(this);
    connect(m_hideTimer, &QTimer::timeout, [=](){this->hide(); m_hideTimer->stop();});
}

baseWidget::~baseWidget()
{
    if (m_gsTransOpacity) {
        delete m_gsTransOpacity;
        m_gsTransOpacity = nullptr;
    }
}

void baseWidget::initOpacityGSettings()
{
    const QByteArray idtrans(THEME_UKFD_TRANS);
    if(QGSettings::isSchemaInstalled(idtrans)) {
        m_gsTransOpacity = new QGSettings(idtrans);
    }

    if (!m_gsTransOpacity) {
        m_curTransOpacity = 1;
        return;
    }

    connect(m_gsTransOpacity, &QGSettings::changed, this, [=](const QString &key) {
        if (key == "transparency") {
            QStringList keys = m_gsTransOpacity->keys();
            if (keys.contains("transparency")) {
                m_curTransOpacity = m_gsTransOpacity->get("transparency").toString().toDouble();
                repaint();
            }
        }
    });

    QStringList keys = m_gsTransOpacity->keys();
    if(keys.contains("transparency")) {
        m_curTransOpacity = m_gsTransOpacity->get("transparency").toString().toDouble();
    }
}

void baseWidget::showBaseWindow(){

    //不在任务栏处显示窗口图标
    QString platform = QGuiApplication::platformName();
    if(!platform.startsWith(QLatin1String("wayland"),Qt::CaseInsensitive)) {
        const KWindowInfo info(this->winId(), NET::WMState);
        if (!info.hasState(NET::SkipTaskbar) || !info.hasState(NET::SkipPager)) {
            KWindowSystem::setState(this->winId(), NET::SkipTaskbar | NET::SkipPager);
        }
    }

    kdk::UkuiStyleHelper::self()->removeHeader(this);
    this->show();

}

void baseWidget::paintEvent(QPaintEvent * event)
{
    QPainterPath path;

    QPainter painter(this);
    painter.setOpacity(m_curTransOpacity);  //设置不透明度
    painter.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    painter.setClipping(true);
    painter.setPen(Qt::transparent);

    path.addRoundedRect(this->rect(), 12, 12);
    path.setFillRule(Qt::WindingFill);   //填充规则
    painter.setBrush(this->palette().color(QPalette::Base));
    painter.setPen(Qt::transparent);

    painter.drawPath(path); //绘制底色
    //QFrame::paintEvent(event);
}

bool baseWidget::eventFilter(QObject *watched, QEvent *event)
{
    if (watched == this)
    {
        if(event->type() == QEvent::Enter) {
           qDebug()<<"鼠标进入";
           if(m_hideTimer->isActive()) {
              m_hideTimer->stop();
           }
           return false;

        } else if (event->type() == QEvent::WindowDeactivate)  {

            qDebug()<<"激活外部窗口";
            this->hide();
            return false;
        } else if (event->type() == QEvent::Leave) {
            if(m_hideTimer->isActive())
                m_hideTimer->start(2000);
        }
    }

    if (!isActiveWindow())
    {
        activateWindow();
    }

    return false;


}

