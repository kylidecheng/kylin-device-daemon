#ifndef BASEWIDGET_H
#define BASEWIDGET_H

#include <QWidget>
#include <QGSettings>
#include <QVBoxLayout>

class baseWidget : public QWidget
{
    Q_OBJECT
public:
    explicit baseWidget(QWidget *parent = nullptr);
    virtual ~baseWidget();
    void initOpacityGSettings();
    void showBaseWindow();

    QVBoxLayout * mainWindowVboxLayout;
    QTimer *m_hideTimer;

protected:
    void paintEvent(QPaintEvent * event);
    bool eventFilter(QObject *watched, QEvent *event);

private:
    // QGSettings
    QGSettings *m_gsTransOpacity = nullptr;
    qreal m_curTransOpacity = 1;


};

#endif // BASEWIDGET_H
