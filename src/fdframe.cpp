/*
 * Copyright (C) 2021 KylinSoft Co., Ltd.
 * 
 * Authors:
 *  Yang Min yangmin@kylinos.cn
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/&gt;.
 *
 */

#include <QPainter>
#include <QStyleOption>
#include <QDebug>
#include <QApplication>
#include <QScreen>
#include <QPainterPath>
#include <KWindowEffects>
#include <QGSettings>
#include <QPixmap>
#include <QGraphicsColorizeEffect>
#include <kwindowsystem.h>
#include <kwindowsystem_export.h>
#include <ukuistylehelper/ukuistylehelper.h>

#include "config/xatom-helper.h"
#include "fdframe.h"
#include "UnionVariable.h"

#define THEME_UKFD_TRANS "org.ukui.control-center.personalise"
#define IN_THEME_BLACKLIST 0

FDFrame::FDFrame(QWidget* parent) : QFrame(parent)
{
    setAttribute(Qt::WA_TranslucentBackground);
    initOpacityGSettings();
    installEventFilter(this);

    //窗管协议
    MotifWmHints hints;
    hints.flags = MWM_HINTS_FUNCTIONS|MWM_HINTS_DECORATIONS;
    hints.functions = MWM_FUNC_ALL;
    hints.decorations = MWM_DECOR_BORDER;
    XAtomHelper::getInstance()->setWindowMotifHint(this->winId(), hints);

    m_animLabel = new ClickLabel(this);
    m_animLabel->move(0,0);
    m_animLabel->setFixedSize(320,112);
    m_animLabel->setAutoFillBackground(true);
    setanimColor();
    m_animLabel->installEventFilter(this);
    m_animLabel->setParent(this);
    m_animLabel->show();

    m_ejectLabel = new QLabel(this);
    QString DriveName = getElidedText(m_ejectLabel->font(), tr("eject"), 47);
    m_ejectLabel->setText(DriveName);
    m_ejectLabel->setStyleSheet("QLabel{color: palette(Text);}");
    m_ejectLabel->setParent(this);
    m_ejectLabel->move(328,35);
    m_ejectLabel->setFixedSize(45,40);
    m_ejectLabel->hide();

    m_ejectIcon = new QLabel(this);
    m_ejectIcon->setProperty("useIconHighlightEffect", 0x2);
    m_ejectIcon->setProperty("useButtonPalette", true);
    QPixmap ejectpixmap =QIcon::fromTheme("media-eject-symbolic").pixmap(16,16);
    m_ejectIcon->setPixmap(ejectpixmap);
    m_ejectIcon->setStyleSheet("QLabel{color: palette(PlaceholderText);}");
    m_ejectIcon->move(343,47);
    m_ejectIcon->setFixedSize(16,16);
    m_ejectIcon->show();

//    m_hideFrameTime = new QTimer(this);
//    connect(m_hideFrameTime, SIGNAL(timeout()), this,SLOT(on_hideframe()));
    KWindowEffects::enableBlurBehind(winId(), true);
    kdk::UkuiStyleHelper::self()->removeHeader(this);
}

FDFrame::~FDFrame()
{
    if (m_gsTransOpacity) {
        delete m_gsTransOpacity;
        m_gsTransOpacity = nullptr;
    }
}

void FDFrame::initOpacityGSettings()
{
    const QByteArray idtrans(THEME_UKFD_TRANS);
    if(QGSettings::isSchemaInstalled(idtrans)) {
        m_gsTransOpacity = new QGSettings(idtrans);
    }

    if (!m_gsTransOpacity) {
        m_curTransOpacity = 1;
        return;
    }

    connect(m_gsTransOpacity, &QGSettings::changed, this, [=](const QString &key) {
        if (key == "transparency") {
            QStringList keys = m_gsTransOpacity->keys();
            if (keys.contains("transparency")) {
                m_curTransOpacity = m_gsTransOpacity->get("transparency").toString().toDouble();
                repaint();
            }
        }
    });

    QStringList keys = m_gsTransOpacity->keys();
    if(keys.contains("transparency")) {
        m_curTransOpacity = m_gsTransOpacity->get("transparency").toString().toDouble();
    }
}



int FDFrame::readlabeltWidth()
{
    return this->m_animLabel->width();
}

void FDFrame::setlabeltWidth(int width)
{
   this->m_animLabel->setFixedWidth(width);
    m_ejectIcon->move(width+23,m_mainWindowHigh/2-11);
    if(width == 288)
    {
        m_ejectIcon->setStyleSheet("QLabel{color: palette(Text);}");
        m_ejectIcon->setProperty("useIconHighlightEffect", 0x2);
        m_ejectLabel->show();
    }else {
        m_ejectIcon->setStyleSheet("QLabel{color: palette(PlaceholderText);}");
        m_ejectLabel->hide();
    }
}

void FDFrame::setanimColor()
{
#if !IN_THEME_BLACKLIST
    QGSettings* gSettings = nullptr;
    if (QGSettings::isSchemaInstalled("org.ukui.style")){
         gSettings = new QGSettings("org.ukui.style");
    }
    if(gSettings != nullptr){
       m_currentThemeMode = gSettings->get("styleName").toString();
       connect(gSettings,&QGSettings::changed,this,[=](const QString &key) {
          if((m_currentThemeMode != nullptr)&&(m_animLabel != nullptr)){
            m_currentThemeMode = gSettings->get("styleName").toString();
            if (("ukui-white" == m_currentThemeMode)||("ukui-light" == m_currentThemeMode)){
                m_animLabel->setStyleSheet("background-color:rgb(255,255,255); border-radius:12px; ");
            }else {
                m_animLabel->setStyleSheet("background-color:rgb(13,13,13); border-radius:12px;");
            }
            repaint();
          }
       });
    }


        style()->unpolish(this->m_animLabel);
       if (("ukui-white" == m_currentThemeMode)||("ukui-light" == m_currentThemeMode)){
           m_animLabel->setStyleSheet("background-color:rgb(255,255,255); border-radius:12px; ");
       }else {
           m_animLabel->setStyleSheet("background-color:rgb(13,13,13); border-radius:12px;");
       }
       style()->polish(this->m_animLabel);
#else
    m_animLabel->setStyleSheet("QLabel { background-color:rgb(13,13,13); border-radius:12px; }");
#endif
}


void FDFrame::paintEvent(QPaintEvent * event)
{
    QPainterPath path;

    QPainter painter(this);
    painter.setOpacity(m_curTransOpacity);
    painter.setRenderHint(QPainter::Antialiasing);  // 反锯齿;
    painter.setClipping(true);
    painter.setPen(Qt::transparent);

    path.addRoundedRect(this->rect(), 12, 12);
    path.setFillRule(Qt::WindingFill);
    painter.setBrush(this->palette().color(QPalette::Window));
    painter.setPen(Qt::transparent);
    painter.drawPath(path);
#if !IN_THEME_BLACKLIST
    //setanimColor();
#endif
    QFrame::paintEvent(event);
}

bool FDFrame::eventFilter(QObject *obj, QEvent *event)
{
    static bool bIsEnterFlag = false;
    if(obj == m_animLabel)
    {
        if(event->type() == QEvent::MouseButtonPress)
        {
            qDebug()<<"-----MouseButtonPress------";
            return true;
        }
        if(event->type() == QEvent::MouseButtonRelease)
        {
            qDebug()<<"-----MouseButtonRelease------";
            return true;

        }

    }

    if(event->type() == QEvent::Leave)
    {

        if(bIsEnterFlag){
            m_curTransOpacity-=0.1;
            bIsEnterFlag = false;
        }else{
            bIsEnterFlag = true;
        }

        repaint();
        return true;

    }
    if(event->type() == QEvent::Enter)
    {
        //由于从底部或左侧进入会重复出现两次ENTER事件导致透明度设置异常，故暂先使用以下手段规避问题
        if(!bIsEnterFlag){
            m_curTransOpacity+=0.1;
            bIsEnterFlag = true;
        }else{
            bIsEnterFlag = false;
        }

        repaint();

//        if (m_hideFrameTime->isActive()) {
//          m_hideFrameTime->stop();
//        }
        return true;
    }
    return false;

}

void FDFrame::on_updateSize()
{
    QPropertyAnimation * m_enterAnimation = new QPropertyAnimation(this, "labeltWidth");
    m_enterAnimation->setDuration(300);
    m_enterAnimation->setStartValue(320);
    m_enterAnimation->setEndValue(288);
    m_enterAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

void FDFrame::on_cursorLeave()
{
    QPropertyAnimation * m_leaveAnimation = new QPropertyAnimation(this, "labeltWidth");
    m_leaveAnimation->setDuration(300);
    m_leaveAnimation->setStartValue(288);
    m_leaveAnimation->setEndValue(320);
    m_leaveAnimation->start(QAbstractAnimation::DeleteWhenStopped);
}

void FDFrame::showNoIcon(){

    //不在任务栏处显示窗口图标
    const KWindowInfo info(this->winId(), NET::WMState);
    if (!info.hasState(NET::SkipTaskbar) || !info.hasState(NET::SkipPager)) {
        KWindowSystem::setState(this->winId(), NET::SkipTaskbar | NET::SkipPager);
    }

    this->show();
}

void FDFrame::setEjectOnCentra(int height)
{

    m_ejectIcon->move(343,height/2-11);
    m_ejectLabel->move(328,height/2-22);  //减去字体高度


}

//void FDFrame::on_hideframe()
//{
//    //m_hideFrameTime->stop();
//    //this->hide();

//}
